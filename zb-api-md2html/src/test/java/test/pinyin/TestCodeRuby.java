package test.pinyin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import jodd.io.FileUtil;
import kits.my.BufferKit;
import lombok.val;
import zb.md2html.gen.ZbBase;

public class TestCodeRuby extends ZbBase{
	private final File file = new File("c:/h.txt");
	//高亮
	private final String height = "<span class='kd'>$0</span>";
	//注释
	private final String nodes = "<span class='c1'>{}</span>";
	private final String normal = "<span class='nx'>{}</span>";
	
	
	
//	@Test
	public void test() throws IOException {
		val lines = Arrays.asList(FileUtil.readLines(file));
		val sb = new BufferKit();
		lines.forEach(line->{
			for (String word : line.split(" ")) {
				if("#".equals(word) || "//".equals(word)) {
					line = StrUtil.format(nodes, line);
				}else {
					if(highlight.contains(word)) {
						line = line.replace(word, StrUtil.format(height, word));
					}
					else {
						if(!StrUtil.isBlank(word) && !"=".equals(word)) {
							line = line.replace(word, StrUtil.format(normal, word));	
						}
					}
				}
			}
			sb.append(line);
		});
		sb.toString();
	}
	
	@Test
	public void test2() throws IOException {
		String html = FileUtil.readString(file);
		//进行代码高亮处理
		for (String hl : highlight) {
			html = ReUtil.replaceAll(html, hl, height);
		}
		System.out.println(html);
	}
	
	
	@Before
	public void before() throws IOException {
		
	}
}
