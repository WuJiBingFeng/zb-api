package zb.md2html.entity;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Language {
	private Set<String> lans;
	private String html;
}
