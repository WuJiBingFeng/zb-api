package zb.md2html.entity;

import lombok.Data;
/**
 * 语言包
 * @author Administrator
 *
 */
@Data
public class I18n {
	private String cn;
	private String en;
	private String ja;
	private String ko;
	private String ar;
	private String th;

	public I18n(String cn) {
		super();
		this.cn = cn;
		this.en = "";
		this.ja = "";
		this.ko = "";
		this.ar = "";
		this.th = "";
	}
}