## 获取委托买单或卖单
```request
GET https://trade.zb.cn/api/getOrder?accesskey=youraccesskey&currency=ltc_btc&id=201710122805&method=getOrder&sign=请求加密签名串&reqTime=当前时间毫秒数
```
```response
{
    "currency": "btc",
    "id": "20150928158614292",
    "price": 1560,
    "status": 3,
    "total_amount": 0.1,
    "trade_amount": 0,
    "trade_price" : 6000,
    "trade_date": 1443410396717,
    "trade_money": 0,
    "type": 0,
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
method | String | 直接赋值getOrder
accesskey | String | accesskey
id | long | 委托挂单号
currency | String | 交易币种_计价币种
sign | String | 请求加密签名串
reqTime | long | 当前时间毫秒数

```resdata
currency : 交易类型
id : 委托挂单号
price : 单价
status : 挂单状态（1：取消，2：交易完成，0/3：待成交/待成交未交易部份）
total_amount : 挂单总数量
trade_amount : 已成交数量
trade_date : 委托时间
trade_money : 已成交总金额
type : 挂单类型 1/0[buy/sell]
```

```java
public void getOrder() {
	//测试apiKey:ce2a18e0-dshs-4c44-4515-9aca67dd706e
	//测试secretKey:c11a122s-dshs-shsa-4515-954a67dd706e
	//加密类:https://github.com/zb2017/api/blob/master/zb_netty_client_java/src/main/java/websocketx/client/EncryDigestUtil.java
	//secretKey通过sha1加密得到:86429c69799d3d6ac5da5c2c514baa874d75a4ba
	String digest = EncryDigestUtil.digest("c11a122s-dshs-shsa-4515-954a67dd706e");
	//参数按照ASCII值排序
	String paramStr = "accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&currency=bch_qc&id=20180522105585216&method=getOrder";
	//sign通过HmacMD5加密得到:c25289ab4f1f7027b6aaf83b7bd5c50b
	String sign = EncryDigestUtil.hmacSign(paramStr, digest);
	//组装最终发送的请求url
	String finalUrl = "https://trade.zb.cn/api/getOrder?accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&currency=bch_qc&id=20180522105585216&method=getOrder&reqTime=1539942326049&sign=c25289ab4f1f7027b6aaf83b7bd5c50b";
	//取得返回结果json
	String resultJson = HttpRequest.get(finalUrl).send().bodyText();
}
```

```python
import hashlib
import struct
import time
import requests

class ZApi:
    def __init__(self, access_key, secret_key):
        self._access_key_ = access_key
        self._secret_key_ = secret_key
        self._markets_ = self.markets()
        if len(self._markets_) < 1:
            raise Exception("Get markets status failed")

    def getOrder(self, market, type, amount, price):
        MyUrl = 'https://trade.zb.cn/api/getOrder'
        params = 'accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&currency=bch_qc&id=20180522105585216&method=getOrder'
        return self.call_api(url=MyUrl, params=params)

    def call_api(self, url, params=''):
        full_url = url
        if params:
            #sha_secret=86429c69799d3d6ac5da5c2c514baa874d75a4ba
            sha_secret = self.digest(self._secret_key_)
            #sign = c25289ab4f1f7027b6aaf83b7bd5c50b
            sign = self.hmac_sign(params, sha_secret)
            req_time = int(round(time.time() * 1000))
            params += '&sign=%s&reqTime=%d' % (sign, req_time)
            #full_url = https://trade.zb.cn/api/getOrder?accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&currency=bch_qc&id=20180522105585216&method=getOrder&reqTime=1540295995519&sign=c25289ab4f1f7027b6aaf83b7bd5c50b
            full_url += '?' + params
        result = {}
        while True:
            try:
                r = requests.get(full_url, timeout=2)
            except Exception:
                time.sleep(0.5)
                continue
            if r.status_code != 200:
                time.sleep(0.5)
                r.close()
                continue
            else:
                result = r.json()
                r.close()
                break
        return result

    @staticmethod
    def fill(value, lenght, fill_byte):
        if len(value) >= lenght:
            return value
        else:
            fill_size = lenght - len(value)
        return value + chr(fill_byte) * fill_size

    @staticmethod
    def xor(s, value):
        slist = list(s.decode('utf-8'))
        for index in range(len(slist)):
            slist[index] = chr(ord(slist[index]) ^ value)
        return "".join(slist)

    def hmac_sign(self, arg_value, arg_key):
        keyb = struct.pack("%ds" % len(arg_key), arg_key.encode('utf-8'))
        value = struct.pack("%ds" % len(arg_value), arg_value.encode('utf-8'))
        k_ipad = self.xor(keyb, 0x36)
        k_opad = self.xor(keyb, 0x5c)
        k_ipad = self.fill(k_ipad, 64, 54)
        k_opad = self.fill(k_opad, 64, 92)
        m = hashlib.md5()
        m.update(k_ipad.encode('utf-8'))
        m.update(value)
        dg = m.digest()

        m = hashlib.md5()
        m.update(k_opad.encode('utf-8'))
        subStr = dg[0:16]
        m.update(subStr)
        dg = m.hexdigest()
        return dg
  
    def digest(self, arg_value):
        value = struct.pack("%ds" % len(arg_value), arg_value.encode('utf-8'))
        h = hashlib.sha1()
        h.update(value)
        dg = h.hexdigest()
        return dg
```