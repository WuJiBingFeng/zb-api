package core.zb.tools;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import com.jfinal.kit.PathKit;

import jodd.io.FileUtil;
import jodd.io.findfile.FindFile;
import jodd.template.MapTemplateParser;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class MdTools {
	private @NonNull String path;

	public void gen() throws IOException {
		path += "\\src\\main\\webapp\\statics\\md\\";
		// md 转 html模板
		String template = FileUtil.readString(PathKit.getPath(MdTools.class) + "\\md2html.template");

		HashMap<String, String> params = new HashMap<>();
		Arrays.asList(lan.values()).forEach(lan -> {
			FindFile.create().searchPath(path + "source_" + lan.name() + "\\").findAll().forEach(md -> {
				log.info("转换{} ing...", md.getName());
				try {
					String content = FileUtil.readString(md);
					params.put("content", content);
					String html = new MapTemplateParser().of(params).parse(template);
					String f = path + "doc_" + lan.name() + "\\"
							+ md.getName().substring(0, md.getName().lastIndexOf(".")) + ".html";
					FileUtil.writeString(f, html);
				} catch (IOException e) {
					e.printStackTrace();
				}
				log.info("转换{} ing完成", md.getName());
			});
		});
	}

	/** 用于匹配语言包 */
	private enum lan {
		en, cn
	}

	public static void main(String[] args) throws IOException {
		// 本地static项目
		String path = "E:\\workspace_zb20180614\\zb_statics_mvn";
		MdTools mdTools = new MdTools(path);
		mdTools.gen();
	}
}
